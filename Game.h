#pragma once
#ifndef GAME_H
#define GAME_H

#include "Consts.h"
#include "Pieces.h"

double playerFallSpeed = 1;
double opponentFallSpeed = 2;

bool menu = true;
bool gameStarted = false;
bool multiPlayer = false;


bool fastFall = false;

bool gameOver = false;
bool pieceAlreadyChanged = false;

int game[GAME_WIDTH][GAME_HEIGHT] = { 0 };

Piece current;
Piece store;

double score = 0;
int nbRotation = 0;

int nextPiecesID[3] = { 0 };
double elapsedTime = 0;

double downMovement = 1;

int opponentGame[GAME_WIDTH][GAME_HEIGHT] = { 0 };
Piece opponentCurrent;

double opponentScore = 0;

int nextOpponentPiecesID[3] = { 0 };
double opponentElapsedTime = 0;
double opponentDownMovement = 1;
int opponentMovements = 0;
int lastOpponentMovements = 0;
int opponentNbRotation = 0;

int opponentXTarget = 0;
int opponentYTarget = 0;
int opponentRotationTarget = 0;
double opponentScorePlace = 0;

#endif //GAME_H