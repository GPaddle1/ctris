#pragma once

#ifndef CONSTS_H
#define CONSTS_H

#define PIECES_NB 7

#define DEFAULT_SIZE 32

//#define DEBUG 1

#ifdef DEBUG
#define GAME_WIDTH 6
#define GAME_HEIGHT 23
#else
#define GAME_WIDTH 10
#define GAME_HEIGHT 20
#endif // DEBUG

#define WINDOW_WIDTH 940
#define WINDOW_HEIGHT 700

#define GAME_X_ORIGIN 30
#define GAME_Y_ORIGIN 30

#define HINT_X 12

#define DEFAULT_SCORE_VALUE 100

#define CHAR_PER_LINE 16

#define LARGEST_WIDTH 4
#define LARGEST_HEIGHT 2

#define DEFAULT_THREE_NEXT_PIECES_Y 10
#define HINT_PIECES_OFFSET 3

#define NB_ITEM_MENU 3

#define OPPONENT_MAX_MOVEMENTS 4

const int CHAR_WIDTH = DEFAULT_SIZE - 14;


#endif //CONSTS_H