#pragma once

#ifndef PIECE_H
#define PIECE_H

#include "Consts.h"

typedef struct {
	int** array;
	int width;
	int	x, y;
	int	id; // coin superieur gauche
} Piece;

const Piece piecesArray[PIECES_NB] = {
	{(int* []) { (int[]) { 0,1,1 },	(int[]) { 1,1,0 },	(int[]) { 0,0,0 } },	3,0,0,1},			//S_shape     
	{(int* []){(int[]) { 2,2,0 },	(int[]) { 0,2,2 },	(int[]) { 0,0,0 }},		3,0,0,2},			//Z_shape     
	{(int* []){(int[]) { 0,3,0 },	(int[]) { 3,3,3 },	(int[]) { 0,0,0 }},		3,0,0,3},			//T_shape     
	{(int* []){(int[]) { 0,0,4 },	(int[]) { 4,4,4 },	(int[]) { 0,0,0 }},		3,0,0,4},			//L_shape     
	{(int* []){(int[]) { 5,0,0 },	(int[]) { 5,5,5 },	(int[]) { 0,0,0 }},		3,0,0,5},			//ML_shape    
	{(int* []){(int[]) { 6,6 },		(int[]) { 6,6 }},		2,0,0,6},								//SQ_shape
	{(int* []){(int[]) { 7,7,7,7 },	(int[]) { 0,0,0,0 },	(int[]) { 0,0,0,0 },	(int[]) { 0,0,0,0 }},	4,0,0,7}	//R_shape
};

const Piece opponentPiecesArray[PIECES_NB] = {
	{(int* []) { (int[]) { 0,1,1 },	(int[]) { 1,1,0 },	(int[]) { 0,0,0 } },	3,0,0,1},			//S_shape     
	{(int* []){(int[]) { 2,2,0 },	(int[]) { 0,2,2 },	(int[]) { 0,0,0 }},		3,0,0,2},			//Z_shape     
	{(int* []){(int[]) { 0,3,0 },	(int[]) { 3,3,3 },	(int[]) { 0,0,0 }},		3,0,0,3},			//T_shape     
	{(int* []){(int[]) { 0,0,4 },	(int[]) { 4,4,4 },	(int[]) { 0,0,0 }},		3,0,0,4},			//L_shape     
	{(int* []){(int[]) { 5,0,0 },	(int[]) { 5,5,5 },	(int[]) { 0,0,0 }},		3,0,0,5},			//ML_shape    
	{(int* []){(int[]) { 6,6 },		(int[]) { 6,6 }},		2,0,0,6},								//SQ_shape
	{(int* []){(int[]) { 7,7,7,7 },	(int[]) { 0,0,0,0 },	(int[]) { 0,0,0,0 },	(int[]) { 0,0,0,0 }},	4,0,0,7}	//R_shape
};

#endif //PIECE_H