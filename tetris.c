#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tetris.h"


SDL_Window* pWindow = NULL;
SDL_Surface* win_surf = NULL;
SDL_Surface* plancheSprites = NULL;
SDL_Surface* spritesASCII = NULL;

void init()
{
	pWindow = SDL_CreateWindow("Tetris", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	win_surf = SDL_GetWindowSurface(pWindow);
	plancheSprites = SDL_LoadBMP("../Tetris_sprites.bmp");
	SDL_SetColorKey(plancheSprites, true, 0);  // 0: 00/00/00 noir -> transparent
	spritesASCII = SDL_LoadBMP("../Tetris_ascii.bmp");
	SDL_SetColorKey(spritesASCII, true, 0);  // 0: 00/00/00 noir -> transparent

	srand(time(NULL));

	initThreeNextPieces();

	store.id = -1;
	resetPiece();
	resetOpponentPiece();
}

void displayText(char* text, const int xOffset, const int yOffset) {

	for (int i = 0; i < (strlen(text)); i++)
	{
		SDL_Rect dest = { 0,0,0,0 };

		dest.x = (text[i] - ' ') % CHAR_PER_LINE;
		dest.y = (text[i] - ' ') / CHAR_PER_LINE;

		SDL_Rect srcLetter = { dest.x * DEFAULT_SIZE, dest.y * DEFAULT_SIZE, DEFAULT_SIZE, DEFAULT_SIZE };

		dest.x = xOffset + i * CHAR_WIDTH;
		dest.y = yOffset;

		SDL_BlitSurface(spritesASCII, &srcLetter, win_surf, &dest);
	}
}

void displayRandomInt(const int xOffset, const int yOffset) {
	displayInt(rand() % 1000, xOffset, yOffset, true);
}

void displayInt(int value, const int xOffset, const int yOffset, bool firstUse) {
	if (!firstUse && value == 0)
	{
		return;
	}

	if (value > 0)
	{
		displayInt(value / 10, xOffset, yOffset, false);
	}

	char valueToDisplay = (value % 10) + '0';
	int xPosition = 0;

	if (value) {
		xPosition = (int)floor(log10(abs(value)));
	}

	displayText(&valueToDisplay, xOffset + xPosition * CHAR_WIDTH, yOffset);
}

void drawTextBox(char* text, int y, bool isValidated)
{
	SDL_Rect dest = { 0,0,0,0 };

	int lengthOfText = strlen(text);
	int middlePosition = WINDOW_WIDTH / 2;

	//LeftCorner

	dest.y = DEFAULT_SIZE * y;

	dest.x = (middlePosition - CHAR_WIDTH * (lengthOfText / 2 + 1));
	SDL_BlitSurface(plancheSprites, &leftBoxCorner[isValidated], win_surf, &dest);

	//MiddlePart
	for (int x = 0; x < lengthOfText / 2 + 1; x++)
	{
		dest.x = (middlePosition + CHAR_WIDTH * x);
		SDL_BlitSurface(plancheSprites, &centralBox[isValidated], win_surf, &dest);

		dest.x = (middlePosition - CHAR_WIDTH * x);
		SDL_BlitSurface(plancheSprites, &centralBox[isValidated], win_surf, &dest);
	}

	//RightCorner

	dest.x = (middlePosition + CHAR_WIDTH * (lengthOfText / 2 + 1));
	SDL_BlitSurface(plancheSprites, &rightBoxCorner[isValidated], win_surf, &dest);

	displayText(text, (int)(middlePosition - CHAR_WIDTH * (lengthOfText / 2 - 0.5)), DEFAULT_SIZE * y);
}

void displayMenu()
{
	int y = 4;
	int index = 0;

	drawTextBox("Jouer", y, validateItemMenu == index++);
	y += 2;
	drawTextBox("Duel", y, validateItemMenu == index++);
	y += 2;
	drawTextBox("Quitter", y, validateItemMenu == index++);

}

// fonction qui met � jour la surface de la fenetre "win_surf"
// dt duree derniere frame
void draw(double delta_t)
{
	// remplit le fond
	fillBackground();


#ifdef DEBUG
	for (int i = 0; i < GAME_WIDTH; i++)
	{
		displayInt(i, (i + 1) * DEFAULT_SIZE, 0, true);
	}

	for (int i = 0; i < GAME_HEIGHT; i++)
	{
		displayInt(i, 0, (i + 1) * DEFAULT_SIZE, true);
	}
	gameStarted = true;
	menu = false;
#endif

	drawGame();

	if (menu)
	{
		displayMenu();
	}

	displayCurrentPiece();

	const int NEXT_PIECE_Y = 50;
	const int TEXTS_X = 370;

	displayText("Score :", TEXTS_X, NEXT_PIECE_Y + 0 * DEFAULT_SIZE);
	displayInt((int)score, TEXTS_X, NEXT_PIECE_Y + 1 * DEFAULT_SIZE, true);


	if (multiPlayer)
	{
		displayOpponentCurrentPiece();
		displayText("Score :", TEXTS_X, NEXT_PIECE_Y + 2 * DEFAULT_SIZE);
		displayInt((int)opponentScore, TEXTS_X + (7 + 1) * CHAR_WIDTH, NEXT_PIECE_Y + 2 * DEFAULT_SIZE, true);
	}


	if (gameOver)
	{

		char* gameOverText = opponentScore >= score ? "Game over" : "Vous avez gagne !";

		displayText(gameOverText, DEFAULT_SIZE * 8, DEFAULT_SIZE * 10);
		displayText("'Entrer' pour rejouer", DEFAULT_SIZE * 6, DEFAULT_SIZE * 11);
	}
	else if (gameStarted)
	{
		displayHoldPiece();

		displayThreeNextPieces();

		displayText("Reserve :", TEXTS_X, NEXT_PIECE_Y + (2 + multiPlayer) * DEFAULT_SIZE);

		displayText("Prochaines", TEXTS_X, NEXT_PIECE_Y + (6 + multiPlayer) * DEFAULT_SIZE);
		displayText("pieces :", TEXTS_X, NEXT_PIECE_Y + (6 + 1 + multiPlayer) * DEFAULT_SIZE);
	}

}

void fillBackground()
{
	for (int y = 0; y < win_surf->h; y += DEFAULT_SIZE)
	{
		fillBackgroundLine(y);
	}
}

void fillBackgroundLine(int y)
{
	SDL_Rect dest = { 0,0,0,0 };
	for (int x = 0; x < win_surf->w; x += DEFAULT_SIZE)
	{
		dest.x = x;
		dest.y = y;
		SDL_BlitSurface(plancheSprites, &srcBg, win_surf, &dest);
	}
}

void drawGame()
{

	for (int y = 0; y < GAME_HEIGHT; y++)
	{
		drawGameLine(y, 0, game);
		if (multiPlayer)
		{
			drawGameLine(y, 17, opponentGame);
		}
	}

}

void drawGameLine(int y, int xOffset, int gameBoard[GAME_WIDTH][GAME_HEIGHT])
{
	SDL_Rect dest = { 0,0,0,0 };
	for (int x = 0; x < GAME_WIDTH; x++)
	{
		dest.x = (xOffset + x) * DEFAULT_SIZE + GAME_X_ORIGIN;
		dest.y = y * DEFAULT_SIZE + GAME_Y_ORIGIN;
		int value = gameBoard[x][y];
		if (!value)
		{
			SDL_BlitSurface(plancheSprites, &srcGameBg, win_surf, &dest);
		}
		else
		{
			SDL_BlitSurface(plancheSprites, &srcSquares[value - 1], win_surf, &dest);
		}
	}
}

void displayHoldPiece()
{
	if (store.id != -1)
	{
		displayPiece(HINT_X, 4 + multiPlayer, store.id - 1, true);
	}
}

void displayThreeNextPieces()
{
	displayPiece(HINT_X, DEFAULT_THREE_NEXT_PIECES_Y + (multiPlayer + -1) * HINT_PIECES_OFFSET, nextPiecesID[0], true);
	displayPiece(HINT_X, DEFAULT_THREE_NEXT_PIECES_Y + (multiPlayer + 0) * HINT_PIECES_OFFSET, nextPiecesID[1], true);
	displayPiece(HINT_X, DEFAULT_THREE_NEXT_PIECES_Y + (multiPlayer + 1) * HINT_PIECES_OFFSET, nextPiecesID[2], true);
}

void displayCurrentPiece()
{
	int currentX = current.x;
	int currentY = current.y;

	displayPiece(currentX, currentY, current.id - 1, true);
}

void displayOpponentCurrentPiece()
{
	int currentX = opponentCurrent.x;
	int currentY = opponentCurrent.y;

	displayPiece(17 + currentX, currentY, opponentCurrent.id - 1, false);
}

void displayRandomPiece(const int xPosition, const int yPosition)
{
	displayPiece(xPosition, yPosition, rand() % PIECES_NB, true);
}

int getXOffset(Piece piece) {
	int XOffset = 0;
	for (int y = 0; y < piece.width; y++)
	{
		//printf("\n");
		bool lineEmpty = true;
		for (int x = 0; x < piece.width && lineEmpty; x++)
		{
			//printf("%d", piece.array[x][y]);
			if (piece.array[x][y])
			{
				lineEmpty = false;
			}
		}
		if (lineEmpty)
		{
			XOffset++;
		}
		else
		{
			break;
		}
		//printf("\n");
	}
	return XOffset;
}

int getYOffset(Piece piece) {
	int YOffset = 0;
	for (int y = 0; y < piece.width; y++)
	{
		//printf("\n");
		bool lineEmpty = true;
		for (int x = 0; x < piece.width && lineEmpty; x++)
		{
			//printf("%d", piece.array[x][y]);
			if (piece.array[y][x])
			{
				lineEmpty = false;
			}
		}

		if (lineEmpty)
		{
			YOffset++;
		}
		else
		{
			break;
		}
		//printf("\n");
	}


	return YOffset;
}


void displayPiece(const int xPosition, const int yPosition, const int id, bool isHumanPlayer)
{
	// display single square
	SDL_Rect dstSquare = { GAME_X_ORIGIN + xPosition, GAME_Y_ORIGIN + yPosition, 0, 0 };

	Piece pieceToDisplay = isHumanPlayer ? piecesArray[id] : opponentPiecesArray[id];

	const int CURRENT_WIDTH = pieceToDisplay.width;
	const int CURRENT_HEIGHT = getHeight(pieceToDisplay);

	const int startX = getXOffset(pieceToDisplay);
	const int startY = getYOffset(pieceToDisplay);

	for (int x = startX; x < CURRENT_WIDTH; x++)
	{
		for (int y = startY; y < CURRENT_WIDTH; y++)
		{
			const int CURRENT_VALUE = pieceToDisplay.array[y][x];
			if (CURRENT_VALUE)
			{
				dstSquare.x = GAME_X_ORIGIN + (xPosition + x) * DEFAULT_SIZE;
				dstSquare.y = GAME_Y_ORIGIN + (yPosition + y) * DEFAULT_SIZE;

				SDL_BlitSurface(plancheSprites, &srcSquares[id], win_surf, &dstSquare);
			}
		}
	}
}

void rotatePiece(Piece pieceToRotate, bool isHumanPlayer) {
	if (isHumanPlayer)
	{
		nbRotation++;
	}
	else {
		opponentNbRotation++;
	}

	int** tempArray;
	tempArray = (int**)malloc(pieceToRotate.width * sizeof(int*));

	for (int x = 0; x < pieceToRotate.width; x++)
	{
		tempArray[x] = (int*)malloc(pieceToRotate.width * sizeof(int));

		for (int j = 0, k = pieceToRotate.width - 1; j < pieceToRotate.width; j++, k--) {
			tempArray[x][j] = pieceToRotate.array[k][x];
		}
	}

	for (int x = 0; x < pieceToRotate.width; x++)
	{
		for (int y = 0; y < pieceToRotate.width; y++) {
			pieceToRotate.array[x][y] = tempArray[x][y];
		}
	}

	for (int i = 0; i < pieceToRotate.width; i++) {
		free(tempArray[i]);
	}
	free(tempArray);
}

void rotateCurrent() {
	rotatePiece(current, true);
}

int getSize(Piece piece, bool isHeight) {
	int size = 0;
	for (int x = 0; x < piece.width; x++)
	{
		bool lineEmpty = true;
		for (int y = 0; y < piece.width && lineEmpty; y++)
		{
			if (piece.array[isHeight ? x : y][isHeight ? y : x])
			{
				lineEmpty = false;
				size++;
			}
		}
	}
	return size;
}

int getHeight(Piece piece) {
	return getSize(piece, true);
}

int getHeightOfCurrent() {
	return getHeight(current);
}

int getWidth(Piece piece) {
	return getSize(piece, false);
}

int getWidthOfCurrent() {
	return getWidth(current);
}

bool isPressed(bool value) {
	return !value && true;
}

void undoRotation() {

	rotateCurrent();
	rotateCurrent();
	rotateCurrent();
}

// fonction callbakc clavier 
// keys = tableau de bool  : touches enfoncees ?
void keyboard(const Uint8* keys, double delta_t)
{
	downMovement = 1;

	if (keys[SDL_SCANCODE_LEFT])
	{
		bool canGoLeft = isPressed(lookingKeys[0]) && !isColliding(current.x - 1, current.y, game, current);
		if (canGoLeft)
		{
			current.x--;
		}
	}

	if (keys[SDL_SCANCODE_RIGHT])
	{
		bool canGoRight = isPressed(lookingKeys[1]) && !isColliding(current.x + 1, current.y, game, current);
		if (canGoRight)
		{
			current.x++;
		}
	}

	if (keys[SDL_SCANCODE_UP])
	{
		if (isPressed(lookingKeys[2]))
		{
			if (menu)
			{
				updateMenuSelection(-1);
			}
			else
			{
				rotateCurrent();

				if (isColliding(current.x, current.y, game, current))
				{
					undoRotation();
				}
			}
		}
	}

	if (keys[SDL_SCANCODE_DOWN])
	{
		if (menu && isPressed(lookingKeys[3]))
		{
			updateMenuSelection(1);
		}
		else
		{
			downMovement = 5;
			fastFall = true;
		}
	}
	else if (fastFall) {
		fastFall = false;
	}

	if (keys[SDL_SCANCODE_SPACE])
	{
		bool wantToStockPiece = isPressed(lookingKeys[4]) && !pieceAlreadyChanged;
		if (wantToStockPiece)
		{
			resetRotation(true);
			storePiece();
		}
	}

	if (keys[SDL_SCANCODE_RETURN])
	{
		if (isPressed(lookingKeys[5]))
		{

			if (menu)
			{
				switch (validateItemMenu)
				{
				case 0:
					gameStarted = true;
					menu = false;
					break;
				case 1:
					gameStarted = true;
					menu = false;
					multiPlayer = true;
					break;
				case 2:
					SDL_Quit();
					break;
				default:
					break;
				}
			}
			else if (gameOver)
			{
				gameOver = false;
				resetGame();
			}
		}
	}
	updateLookingKeys(keys);
}

void updateMenuSelection(int change) {
	validateItemMenu += change;
	if (validateItemMenu < 0)
	{
		validateItemMenu += NB_ITEM_MENU;
	}
	else if (validateItemMenu >= NB_ITEM_MENU) {
		validateItemMenu = 0;
	}
}

void resetGame() {

	for (int x = 0; x < GAME_WIDTH; x++)
	{
		resetGameLine(x, game);
		resetGameLine(x, opponentGame);
	}

	score = 0;
	opponentScore = 0;
	elapsedTime = 0;
	initThreeNextPieces();
	resetOpponentPiece();

}

void resetGameLine(int x, int gameBoard[GAME_WIDTH][GAME_HEIGHT])
{
	for (int y = 0; y < GAME_HEIGHT; y++)
	{
		gameBoard[x][y] = 0;
	}
}


void initThreeNextPieces() {
	for (int i = 0; i < 3; i++)
	{
		nextPiecesID[i] = rand() % PIECES_NB;
	}
}

void initOpponentThreeNextPieces() {
	for (int i = 0; i < 3; i++)
	{
		nextOpponentPiecesID[i] = rand() % PIECES_NB;
	}
}


void storePiece() {
	if (!pieceAlreadyChanged)
	{
		if (store.id != -1)
		{
			Piece tmp = current;
			current = store;
			store = tmp;
			pieceAlreadyChanged = true;
		}
		else
		{
			store = current;
			resetPiece();
		}
	}
}

void updateLookingKeys(const Uint8* keys) {
	lookingKeys[0] = keys[SDL_SCANCODE_LEFT];
	lookingKeys[1] = keys[SDL_SCANCODE_RIGHT];
	lookingKeys[2] = keys[SDL_SCANCODE_UP];
	lookingKeys[3] = keys[SDL_SCANCODE_DOWN];
	lookingKeys[4] = keys[SDL_SCANCODE_SPACE];
	lookingKeys[5] = keys[SDL_SCANCODE_RETURN];
}

int main(int argc, char** argv)
{
	Uint64 prev, now = SDL_GetPerformanceCounter(); // timers
	double delta_t;  // duree frame en ms
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		return 1;
	}

	init();

	bool quit = false;
	while (!quit)
	{
		SDL_PumpEvents();

		prev = now;
		now = SDL_GetPerformanceCounter();
		delta_t = (double)((now - prev) / (double)SDL_GetPerformanceFrequency());

		if (gameStarted)
		{
			elapsedTime += delta_t * (downMovement + playerFallSpeed);
			opponentElapsedTime += delta_t * (opponentDownMovement + opponentFallSpeed);

			fastFallAction(delta_t);
			updateCurrent();
			updateOpponent();
		}

		const Uint8* state = SDL_GetKeyboardState(NULL);
		keyboard(state, delta_t);
		quit |= state[SDL_SCANCODE_ESCAPE];
		draw(delta_t);

		SDL_UpdateWindowSurface(pWindow);
	}
	SDL_Quit();
	return 0;
}

void fastFallAction(double delta_t)
{
	if (fastFall)
	{
		score += delta_t * (downMovement + playerFallSpeed) / 4;
	}
}

void updateCurrent() {
	int wantedYValue = (int)(elapsedTime);
	if (isColliding(current.x, current.y + 1, game, current) && wantedYValue != current.y)
	{
		bool collidingTheTop = current.y == 0;

		if (collidingTheTop)
		{
			gameOver = true;
			return;
		}

		storeInGame(true);
		resetPiece();

		int scoreMultiplier = proceedLines(true);

		score += (exp(scoreMultiplier / 2.0) - 1) * DEFAULT_SCORE_VALUE;

		playerFallSpeed += scoreMultiplier / 20.0;
	}
	else if (wantedYValue <= GAME_HEIGHT - getHeightOfCurrent()) {
		current.y = wantedYValue;
	}
	else
	{
		printf("updateCurrent : autre cas\n");
	}
}

void updateOpponent() {
	int wantedYValue = (int)(opponentElapsedTime);

	if (isColliding(opponentCurrent.x, opponentCurrent.y + 1, opponentGame, opponentCurrent) && wantedYValue != opponentCurrent.y)
	{
		bool collidingTheTop = opponentCurrent.y == 0;

		if (collidingTheTop)
		{
			gameOver = true;
			return;
		}

		storeInGame(false);
		resetOpponentPiece();

		int scoreMultiplier = proceedLines(false);

		opponentScore += (exp(scoreMultiplier / 2.0) - 1) * DEFAULT_SCORE_VALUE;
		opponentFallSpeed += scoreMultiplier / 20.0;

	}
	else if (wantedYValue <= GAME_HEIGHT - getHeight(opponentCurrent)) {
		opponentCurrent.y = wantedYValue;
	}
	else
	{
		printf("Opponent : updateCurrent : autre cas\n");
	}

	tacticalOpponentMovement();



	//	randomOpponentMovement();

}

void tacticalOpponentMovement()
{
	if (opponentNbRotation % 4 < opponentRotationTarget)
	{
		rotatePiece(opponentCurrent, false);
	}
	else {
		if (opponentCurrent.x < opponentXTarget)
		{
			opponentCurrent.x++;
		}
		else if (opponentCurrent.x > opponentXTarget)
		{
			opponentCurrent.x--;
		}
		else {
			opponentDownMovement = 2;
		}
	}
}

void randomOpponentMovement()
{
	if (opponentMovements < OPPONENT_MAX_MOVEMENTS)
	{
		opponentMovements++;


		if (!isColliding(opponentCurrent.x - 1, opponentCurrent.y, opponentGame, opponentCurrent) &&
			!isColliding(opponentCurrent.x + 1, opponentCurrent.y, opponentGame, opponentCurrent)) {

			switch (lastOpponentMovements)
			{
			case -1:
				lastOpponentMovements = rand() % 4 >= 1 ? -1 : 1;
				break;
			case 1:
				lastOpponentMovements = rand() % 4 <= 1 ? -1 : 1;
				break;
			default:
				lastOpponentMovements = rand() % 2 >= 1 ? -1 : 1;
				break;
			}

			opponentCurrent.x += lastOpponentMovements;
		}
		opponentFallSpeed = 2;
		rotatePiece(opponentCurrent, false);
	}
}

int proceedLines(bool isHumanPlayer) {
	int lineRemoved = 0;

	for (int line = GAME_HEIGHT - 1; line >= 0; line--)
	{
		if (checkLineFull(line, isHumanPlayer)) {
			validateLine(line, isHumanPlayer);
			line++;
			lineRemoved++;

		}
	}

	return lineRemoved;
}

void validateLine(int line, bool isHumanPlayer) {
	for (int lookingY = line; lookingY >= 0; lookingY--)
	{
		for (int x = 0; x < GAME_WIDTH; x++)
		{
			if (isHumanPlayer) {
				game[x][lookingY] = game[x][lookingY - 1];
			}
			else {
				opponentGame[x][lookingY] = opponentGame[x][lookingY - 1];
			}
		}
	}

	for (int x = 0; x < GAME_WIDTH; x++)
	{
		if (isHumanPlayer) {
			game[x][0] = 0;
		}
		else {
			opponentGame[x][0] = 0;
		}
	}
}

bool checkLineFull(int line, bool isHumanPlayer) {
	for (int x = 0; x < GAME_WIDTH; x++)
	{
		if (!(isHumanPlayer ? game[x][line] : opponentGame[x][line]))
		{
			return false;
		}
	}
	return true;
}

void resetRotation(bool isHumanPlayer) {

	for (int i = (isHumanPlayer ? nbRotation : opponentNbRotation) % 4; i < 4; i++)
	{
		isHumanPlayer ? rotateCurrent() : rotatePiece(opponentCurrent, false);
	}

	if (isHumanPlayer)
	{
		nbRotation = 0;
	}
	else {
		opponentNbRotation = 0;
	}
}

void resetPiece()
{
	resetRotation(true);

#ifdef DEBUG
	current = piecesArray[4]; // Square shape
#else
	current = piecesArray[nextPiecesID[0]];
#endif // DEBUG

	nextPiecesID[0] = nextPiecesID[1];
	nextPiecesID[1] = nextPiecesID[2];
	nextPiecesID[2] = rand() % PIECES_NB;

	current.x = rand() % (GAME_WIDTH - current.width + 1);
	current.y = 0;

	elapsedTime = 0;
	pieceAlreadyChanged = false;
}

void resetOpponentPiece()
{
#ifdef DEBUG
	opponentCurrent = opponentPiecesArray[4]; // Square shape
#else
	opponentCurrent = opponentPiecesArray[rand() % PIECES_NB];
#endif // DEBUG

	resetRotation(false);

	opponentCurrent.x = rand() % (GAME_WIDTH - opponentCurrent.width + 1);
	opponentCurrent.y = 0;

	opponentElapsedTime = 0;
	opponentMovements = 0;


	opponentXTarget = 0;
	opponentYTarget = 0;
	opponentRotationTarget = 0;
	opponentScorePlace = 0;

	//printf("Reset ----------------\n");

	for (int rotation = 0; rotation < 4; rotation++)
	{
		int height = getHeight(opponentCurrent);
		int width = getWidth(opponentCurrent);
		int xOffset = getXOffset(opponentCurrent);

		for (int x = 0; x < GAME_WIDTH - width + 1; x++)
		{
			bool yStop = false;
			for (int y = 0; y < GAME_HEIGHT && !yStop; y++)
			{
				if (isColliding(x- xOffset, y, opponentGame, opponentCurrent))
				{
					double score = getScoreOfCoords(x- xOffset, y, height, width);
					if (score > opponentScorePlace) {
						opponentXTarget = x- xOffset;
						opponentYTarget = y;
						opponentRotationTarget = rotation;
						opponentScorePlace = score;
						//printf("New best choice : (%d;%d) %d %f\n", opponentXTarget, opponentYTarget, opponentRotationTarget, opponentScorePlace);
					}
					yStop = true;
				}
			}
		}
		rotatePiece(opponentCurrent, false);
	}
}

double getScoreOfCoords(int x, int y, int height, int width)
{
	return (y + height) * (y + height > 7 && height > width ? 1.1 : 1.0) * getComplementarity(x, y) * getUnreachableCells(x, y);
}

double getComplementarity(int x, int y)
{
	int nbComplementary = 0;

	int width = getWidth(opponentCurrent);
	int height = getHeight(opponentCurrent);

	int xOffset = getXOffset(opponentCurrent);
	int yOffset = getYOffset(opponentCurrent);

	//printf("[%d %d]\t", width, height);

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int value = opponentGame[x + i][y + j] ^ opponentCurrent.array[j + yOffset][i + xOffset];
			if (value)
			{
				nbComplementary++;
			}
		}
	}
	//printf("%d\t", nbcomplementary);
	if (nbComplementary > width * height - 1)
	{
		return 1.25;
	}
	else {
		return 0.75;
	}
}

double getUnreachableCells(int x, int y)
{
	int nbUnreachable = 0;

	int width = getWidth(opponentCurrent) - getXOffset(opponentCurrent);
	int height = getHeight(opponentCurrent);

	//printf("[%d %d]\t", x, y);

	for (int i = 0; i < width; i++)
	{
		bool jBreak = false;
		for (int j = y + height; j < GAME_HEIGHT && jBreak; j++)
		{
			if (!opponentGame[x + i][j])
			{
				nbUnreachable++;
			}
			else {
				jBreak = true;
			}
		}
	}
	//printf("%d\t", nbUnreachable);
	if (nbUnreachable >= width)
	{
		return 0.75;
	}
	else {
		return 1.25;
	}
}

bool isColliding(int x, int y, int gameBoard[GAME_WIDTH][GAME_HEIGHT], Piece pieceToTest) {


	const int startX = getXOffset(pieceToTest);
	const int startY = getYOffset(pieceToTest);
	const int height = getHeight(pieceToTest);
	const int width = getWidth(pieceToTest);

	x += startX;
	y += startY;

	bool touchTop = y < 0;
	bool touchLeft = x < 0;
	bool touchBottom = y + height > GAME_HEIGHT;
	bool touchRight = x + width > GAME_WIDTH;

	if (touchTop || touchLeft || touchBottom || touchRight)
	{
		return true;
	}

	for (int currentY = 0; currentY < height; currentY++)
	{
		for (int currentX = 0; currentX < width; currentX++)
		{
			bool isCellColliding = gameBoard[x + currentX][y + currentY] && pieceToTest.array[currentY + startY][currentX + startX];

			if (isCellColliding)
			{
				return true;
			}
		}
	}
	return false;
}

void displayGameConsole(int gameBoard[GAME_WIDTH][GAME_HEIGHT])
{
	for (int y = 0; y < GAME_HEIGHT; y++)
	{
		displayLineConsole(y, gameBoard);
		printf("\n");
	}
}

void displayLineConsole(int y, int gameBoard[GAME_WIDTH][GAME_HEIGHT])
{
	for (int x = 0; x < GAME_WIDTH; x++)
	{
		printf("%d", gameBoard[x][y]);
	}
}

void storeInGame(bool isHumanPlayer) {
	int compte = 0;

	for (int y = 0; y < getHeight(isHumanPlayer ? current : opponentCurrent); y++)
	{
		storeLineInGame(y, isHumanPlayer);
	}

	/*
	displayGameConsole();
	*/
}

void storeLineInGame(int y, bool isHumanPlayer)
{
	Piece targetPiece = isHumanPlayer ? current : opponentCurrent;
	for (int x = 0; x < targetPiece.width; x++)
	{
		int targetX = targetPiece.x + x;
		int targetY = targetPiece.y + y + getYOffset(targetPiece);
		if (targetPiece.array[y + getYOffset(targetPiece)][x])
		{
			if (isHumanPlayer)
			{
				game[targetX][targetY] = targetPiece.id;
			}
			else {
				opponentGame[targetX][targetY] = targetPiece.id;
			}
		}
	}
}

