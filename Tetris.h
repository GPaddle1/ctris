﻿#pragma once

#ifndef TETRIS_H
#define TETRIS_H

#include "Pieces.h"
#include "Drawings.h"
#include "Consts.h"
#include "Game.h"

int validateItemMenu = 0;

bool lookingKeys[6] = { false }; //Left, Right, Up, Down, Space, Enter

// Global function
void init();
/*
Fonction qui met � jour la surface de la fenetre "win_surf"
delta_t duree derniere frame*/
void draw(double delta_t);

// Functions to display text and int
void displayRandomInt(const int xOffset, const int yOffset);
void displayInt(int value, const int xOffset, const int yOffset, bool firstUse);
void displayText(char* text, const int xOffset, const int yOffset);

// Functions to display pieces
void displayPiece(const int xPosition, const int yPosition, const int id, bool isHumanPlayer);
void displayCurrentPiece();
void displayOpponentCurrentPiece();
void displayRandomPiece(const int xPosition, const int yPosition);
void displayHoldPiece();
void displayThreeNextPieces();

// Game UI
void drawGame();
void fillBackground();
void fillBackgroundLine(int y);
void drawGameLine(int y, int xOffset, int gameBoard[GAME_WIDTH][GAME_HEIGHT]);
void drawTextBox(char* text, int y, bool isValidated);
void displayMenu();


// Current piece actions
void updateCurrent();
void resetPiece();
void storeInGame(bool isHumanPlayer);
void storeLineInGame(int y, bool isHumanPlayer);

// Opponent actions
void updateOpponent();
void tacticalOpponentMovement();
void randomOpponentMovement();
void resetOpponentPiece();

double getScoreOfCoords(int x, int y, int height, int width);

double getComplementarity(int x, int y);
double getUnreachableCells(int x, int y);


// Pieces actions
void storePiece();
void resetGame();
void resetGameLine(int x, int gameBoard[GAME_WIDTH][GAME_HEIGHT]);
void initThreeNextPieces();

void rotatePiece(Piece pieceToRotate, bool isHumanPlayer);
void rotateCurrent();
void undoRotation();
void resetRotation();

// Console display

void displayGameConsole(int gameBoard[GAME_WIDTH][GAME_HEIGHT]);
void displayLineConsole(int y, int gameBoard[GAME_WIDTH][GAME_HEIGHT]);

// Keyboard functions
bool isPressed(bool value);
void updateLookingKeys(const Uint8* keys);

void fastFallAction(double delta_t);

void updateMenuSelection(int change);

// Pieces management
int getSize(Piece piece, bool isHeight);

int getHeight(Piece piece);
int getHeightOfCurrent();

int getHeight(Piece piece);
int getHeightOfCurrent();

int getXOffset(Piece piece);
int getYOffset(Piece piece);

// Game management
int proceedLines(bool isHumanPlayer);
bool isColliding(int x, int y, int gameBoard[GAME_WIDTH][GAME_HEIGHT], Piece pieceToTest);
bool checkLineFull(int line, bool isHumanPlayer);
void validateLine(int line, bool isHumanPlayer);

#endif // TETRIS_H
